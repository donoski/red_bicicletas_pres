var express = require('express');
var router = express.Router();
var reservaController = require('../controllers/ReservaController');

router.get('/', reservaController.list); 
router.post('/:id/delete', reservaController.delete_post);
router.get('/:id/update', reservaController.update_get);
router.post('/:id/update', reservaController.update_post);

module.exports = router;

