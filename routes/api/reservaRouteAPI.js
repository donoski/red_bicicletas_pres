var express = require('express');
var router = express.Router();
var reservaControllerAPI = require('../../controllers/api/reservaControllerAPI');

router.get('/', reservaControllerAPI.list);
router.post('/update', reservaControllerAPI.update);

module.exports = router;