var express = require('express');
var router = express.Router();
var usuarioControllerAPI = require('../../controllers/api/usuarioControllerAPI');

router.get('/', usuarioControllerAPI.list);
router.post('/create', usuarioControllerAPI.create);
router.post('/delete', usuarioControllerAPI.delete);
router.post('/reservar', usuarioControllerAPI.reservar);
router.put('/:id/update', usuarioControllerAPI.update);  

module.exports = router;