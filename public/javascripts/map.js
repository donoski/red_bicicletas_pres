var map = L.map('main_map').setView([-37.121191,-56.873840], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, {title: `Bicicleta ${bici.code} \nModelo: ${bici.modelo} \nColor: ${bici.color} `}).addTo(map);
        });
    }
})