CURSO Desarrollo del lado servidor: NodeJS, Express y MongoDB
Proyecto: redBicicletasApp
Tareas calificadas por los compañeros
Este proyecto corresponde al Final de Semana 1 para la aplicacion Red Bicicletas

El proyecto incluye:
    
    - un archivo README (sólo texto) en el repositorio de Bitbucket.
    el archivo actual README_redBicicletasAPP.txt

    - el mensaje de bienvenida a Expres
    se muestra el mensaje al levantar el server con un console.log
    se ve como un Listening on port 3000

    -- el proyecto vinculado con el repositorio de Bitbucket creado previamente.
    URL repositorio: https://bitbucket.org/donoski/red_bicicletas_pres/src/master/

    -- el servidor que se ve correctamente, comparándolo con la versión original.
    se utilizo otro template minimalista de Bootstrap: startbootstrap-scrolling-nav

    -- un mapa centrado en una ciudad.
    Pinamar. Argentina.

    -- marcadores indicando una  ubicación.
    3 bicicletas cargadas por defecto en memoria.

    -- un script npm en el archivo package.json, debajo del “start”, que se llama “devstart” y que levanta el servidor utilizando npdemon.
    se puede levantar la aplicacion con  npm run devstart

    -- un par de bicicletas en la colección que conoce la bicicleta en memoria, con las ubicaciones cercanas al centro del mapa agregado.
    3 bicicletas cargadas por defecto en memoria

    -- una colección del modelo de bicicleta.
    la lista de bicicletas se accede desde el boton LISTAR Bicicletas
    se accede desde alli a las operaciones basicas de las bicicletas cargadas en memoria.

    -- los endpoints de la API funcionando con Postman.
    se probaron los siguientes endpoints
    localhost:3000/api/bicicletas/ 
    localhost:3000/api/bicicletas/create
    localhost:3000/api/bicicletas/:id/update