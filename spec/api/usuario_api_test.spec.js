// spec\api\usuario_api_test.spec.js

var mongoose = require('mongoose');
var Usuario = require('../../models/Usuario');
var Bicicleta = require('../../models/Bicicleta');
var Reserva = require('../../models/Reserva');
var request = require('request');
var server = require('../../bin/www');

describe('Testing Api Usuarios ', function() {
    
    beforeAll((done) => { mongoose.connection.close(done) });

    beforeEach(function(done){
        
        var mongoDB = "mongodb://localhost/testdbAPI";
        mongoose.connect(mongoDB, { useNewUrlParser:true, useUnifiedTopology: true, useCreateIndex: true, });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            Usuario.deleteMany({}, function( err, success ){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success)  {
                    if (err) console.log(err);
                    
                    mongoose.disconnect();
                    done();
                });
            });
        });
    });

    describe('>> API USUARIOS/create', () => {
        it('-> Status 200', (done) => {
            
            var headers = { 'content-type' : 'application/json'};
            
            let vUsuario = {'nombre': 'Juan Prueba', 
                            'email': 'juanprueba@mail.com', 
                            'password': 'password' };

            request.post({
                headers : headers,
                url : 'http://localhost:3000/api/usuarios/create',
                body : JSON.stringify(vUsuario)
            },
            function(error, response, body) {
                expect(response.statusCode).toBe(200); 
                Usuario.find({ 'email': vUsuario.email }, ( err, usuario ) => {
                    if ( err) console.log(err); 
                    expect(usuario[0].nombre).toEqual('Juan Prueba');
                    done();
                });
                
            });
        
        });
    });
 
    describe('>> POST USUARIOS/reservar', () => {
        it('-> Devolver informacion de la reserva', (done) => {
            
            let vUsuario = new Usuario( {   'nombre': 'Juan Prueba', 
                                            'email': 'juanprueba@mail.com', 
                                            'password': 'password' }
                                    );

            vUsuario.save(function (err)  {
                if (err) console.log(err);
            
                let vBicicleta = new Bicicleta( { 'code': 2, 
                                            'color': 'Violeta',
                                            'modelo': 'Montaña' }
                                        );
            
                vBicicleta.save(function (err)  {
                    if (err) console.log(err);
            
                    let headers = {'content-type' : 'application/json'};
                    
                    let vReserva = {
                        desde:      '2020-09-01',
                        hasta:      '2020-09-03',
                        usuario:    vUsuario._id,
                        bicicleta:  vBicicleta._id
                    };
 
                    request.post({
                        headers : headers,
                        url : 'http://localhost:3000/api/usuarios/reservar',
                        body : JSON.stringify(vReserva)
                    },
                    function(error, response, body) {
                        expect(response.statusCode).toBe(200);
                        Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
                            if ( err) console.log(err);
                            done();
                        });
                    }); 
                });
        
            }); 
        });
    });
 
    describe('>> POST USUARIOS/update', () => {
        it('-> Actualizar un usuario y verificar que lo hizo bien.', (done) => {
            
            let vUsuario = new Usuario( {   'nombre': 'Juan Prueba', 
                                            'email': 'juanprueba@mail.com', 
                                            'password': 'password' }
                                    );
            
            vUsuario.save( function(err, user) {
                if( err ) console.log( err );
                Usuario.findById(vUsuario._id, function(err, usuario){
                    if( err ) console.log(err);
                     
                    let vUsuarioUpdate = { 
                        nombre: "Maria Prueba",
                        email: "mismomail@mail.com",
                        password: "mismapassword"
                    };

                    var headers = {'content-type' : 'application/json'};
                     
                    request.put({
                        headers : headers,
                        url : 'http://localhost:3000/api/usuarios/'+ usuario._id + '/update',
                        body :  JSON.stringify(vUsuarioUpdate)
                        
                    },
                    function(error, response, body) { 
                        expect(response.statusCode).toBe(200);
                        Usuario.findById(usuario._id, ( err, usuario) => {
                            if ( err) console.log(err);
                            // console.log(usuario);
                            expect(usuario.nombre).toBe("Maria Prueba");
                            done();
                        });
                    });
                });
            });        
        });
    }); 
});
