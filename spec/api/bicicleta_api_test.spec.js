var Bicicleta = require('../../models/Bicicleta');
var server = require('../../bin/www');
var request = require('request');
var mongoose = require('mongoose');

var base_url = "http://localhost:3000/api/bicicletas";
var mongoDB_url = "mongodb://localhost/testdbAPI";

describe('Bicicleta API', () => {
    
    beforeAll((done) => { mongoose.connection.close(done) });

    beforeEach(function(done) {
        
        mongoose.connect(mongoDB_url, {useUnifiedTopology: true, useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('Conectado a testdbAPI');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            mongoose.disconnect();
            done();
        });
    });

    describe('.GET bicicletas /', () => {
        it('Status 200', (done) => {
            //valida si la entidad esta vacia
            Bicicleta.allBicis(( err, bicis ) => {
                expect(bicis.length).toBe(0);
            });

            // Añadir una bicicleta
            var aBici = new Bicicleta({code:1, color:"verde", modelo:"urbana"});
            Bicicleta.add(aBici, ( err, newBici) => {
                if (err) console.log(err);
            });
            
            request.get(base_url, (error, response, body) => { 
                expect(response.statusCode).toBe(200);
                done();
            });
        });
    });

    describe('.POST bicicletas /create', () => {
        it('.Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var vBici = '{ "code": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng":-54 }';

            request.post({
                headers :   headers,
                url :       base_url + '/create',
                body :      vBici
            },
            function(error, response, body) {
                expect(response.statusCode).toBe(200);
                Bicicleta.findByCode(10, (err, bicicleta) => {
                    if (err) console.log(err); 
                    expect(bicicleta.code).toBe(10);
                    done();
                });                
            });
        })
    });

    describe('.DELETE bicicletas /delete', () => {
        it('.Status 204', (done) => {
            var headers = {'content-type' : 'application/json'};
            var vBici = '{ "id": 10}';
            request.delete({
                headers: headers,
                url:     base_url + '/delete',
                body:    vBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(204); 
                done();
            });
        })
    });
 
    describe('.PUT bicicletas /update', () => {
        it('Update de una bicicleta', (done) => {
            
            var vBici = new Bicicleta( {code: 1, color: "Amarilla", modelo: "urbana"} );
            
            vBici.save(( err, bici ) => {
                if (err) console.log(err);
                
                Bicicleta.findById(bici._id).exec(( err, bicicleta ) => {
                     
                    var headers = {'content-type' : 'application/json'};
                    var vbiciUpdate = '{ "code": 1, "color": "otroColor", "modelo": "Urbano", "lat": -34, "lng": -54 }';

                    request.put({
                        headers:   headers,
                        url:       base_url + '/' + vBici.code + '/update',
                        body:      vbiciUpdate
                    }, function(error, response, body) {
                        expect(response.statusCode).toBe(200);
                        Bicicleta.findByCode(1, ( err, bicicleta) => {
                            if ( err) console.log(err); 
                            expect(bicicleta.color).toBe("otroColor");
                            done();
                        });
                    });
 
                });
            });
        });
    });
}); 