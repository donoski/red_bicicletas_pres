var mongoose = require('mongoose');
var Usuario = require('../../models/Usuario');
var Bicicleta = require('../../models/Bicicleta');
var Reserva = require('../../models/Reserva');
var request = require('request');
var server = require('../../bin/www');

describe('Testing Api Usuarios ', function() {
    
    beforeAll((done) => { mongoose.connection.close(done) });

    beforeEach(function(done){
        
        var mongoDB = "mongodb://localhost/testdbAPI";
        mongoose.connect(mongoDB, { useNewUrlParser:true, useUnifiedTopology: true, useCreateIndex: true, });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            Usuario.deleteMany({}, function( err, success ){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success)  {
                    if (err) console.log(err);
                    
                    mongoose.disconnect();
                    done();
                });
            });
        });
    });
    
    describe(' Reservas /update', () => {
        it('devuelve la información de la reserva', (done) => {
            
            const testUsuario = new Usuario ( { 'nombre': 'Juan Prueba', 
                                                'email': 'juanprueba@mail.com', 
                                                'password': 'password' } 
                                            );
            testUsuario.save();

            let bicicleta = new Bicicleta({"code": 1, "color":"azul","modelo":"urbano"});
            bicicleta.save();

            const headers = {'content-type' : 'application/json'};
            
            const reserva = new Reserva ({
                desde: "2020-08-03",
                hasta: "2020-08-05",
                id: testUsuario._id,
                bici_id: bicicleta._id
            });

            reserva.save( ( err, reserva)=> {
                if(err) console.log(err); 
                
                let updateReserva = {
                    _id: reserva._id,
                    desde: "2020-08-04",
                    hasta: "2020-08-07",
                    usuario: testUsuario._id,
                    bicicleta: bicicleta._id
                };
                request.post({
                    headers : headers,
                    url : 'http://localhost:3000/api/reservas/update',
                    body : JSON.stringify(updateReserva)
                },
                function(error, response, body) {
                    expect(response.statusCode).toBe(200);
                    Reserva.find({_id: reserva._id}).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
                        if ( err) console.log(err);
                        console.log(reservas[0]); 
                        done();
                    });
                });
            });
    
        });
    });

}); 