// spec\models\usuario_test.spec.js

var mongoose = require('mongoose');
var Bicicleta = require('../../models/Bicicleta');
var Usuario = require('../../models/Usuario');
var Reserva = require('../../models/Reserva'); 

var mongoDB = 'mongodb://localhost/testdb';

describe('>>> Testing Usuarios', function() {

    beforeAll((done) => { mongoose.connection.close(done) });

    beforeEach(function(done) {
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error...'));
        db.once('open', function() {
            console.log('Connected to database!');
            done();
        });

    });

    afterEach(function(done) {
        Reserva.deleteMany( {}, function(err, success) {
            if (err) console.log(err);
            Usuario.deleteMany( {}, function(err, success) {
                if (err) console.log(err);
                Bicicleta.deleteMany( {}, function(err, success) {
                    if (err) console.log(err);
                    mongoose.disconnect();
                    done();
                });
            });
        });
    });

    describe('>> USUARIO/reservar', () => {
        it('-> debe existir la reserva...', (done) => {
            
            const testUsuario = new Usuario ( { 'nombre': 'Juan Prueba', 
                                                'email': 'juanprueba@mail.com', 
                                                'password': 'password' } 
                                            );
            testUsuario.save();
            
            const testBicicleta = new Bicicleta( {  code: 1, 
                                                    color: "Verde", 
                                                    modelo: "Urbana" }
                                                );
            testBicicleta.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate() + 1);

            testUsuario.reservar(testBicicleta.id, hoy, mañana, function(err, reserva) {
                
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function (error, reservas) {
                    
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(testUsuario.nombre);
                    done();

                });

            });
        });
    });

});
