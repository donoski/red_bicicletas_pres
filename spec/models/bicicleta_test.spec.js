// spec\models\bicicleta_test.spec.js

var mongoose = require('mongoose');
var Bicicleta = require('../../models/Bicicleta');

var mongoDB = 'mongodb://localhost/testdb';

describe('Testing Bicicletas', function() {

    beforeAll((done) => { mongoose.connection.close(done) });

    beforeEach(function(done) {
        
        mongoose.connect(mongoDB, {useUnifiedTopology: true, useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('MongoDB is connected!');
            done();
        }) ;
        
    });
 
    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) { console.log(err); }
            mongoose.disconnect();
            done();
        });
    }); 

    describe('Bicicleta.createInstance', () => {
        it('.crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, 'verde', 'urbana', [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1); 
        });
    });
 
    describe('Bicicleta.allBicis', () => {
        it('.comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            var vBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            Bicicleta.add(vBici, function(err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(vBici.code);

                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var vBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
                Bicicleta.add(vBici, function(err, newBici) {
                    if (err) console.log(err);

                    var vBici2 = new Bicicleta({code: 2, color: "roja", modelo: "urbana"});
                    Bicicleta.add(vBici2, function(err, newBici) {
                    
                        if (err) console.log(err);

                        Bicicleta.findByCode(1, function(err, rBicicleta) {
                            expect(rBicicleta.code).toBe(vBici.code);
                            expect(rBicicleta.color).toBe(vBici.color);
                            expect(rBicicleta.modelo).toBe(vBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });

 
});
  