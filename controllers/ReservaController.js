var Reserva = require('../models/Reserva');
var Bicicleta = require('../models/Bicicleta');
var Usuario = require('../models/Usuario');

exports.list = function(req, res) {
 
    return Reserva
    .find({})
    .populate('bicicleta')
    .populate('usuario')
    .exec(function(err, reservas) {
        if(err) console.log(err); 
        res.render('reservas/index', { listReservas: reservas }); 
    }); 
    
}
 
exports.delete_post = function(req, res) {
    Reserva.removeById(req.params.id, function(err) {
        res.redirect('/reservas');
    });
}

 
exports.update_get = function(req, res) { 

    var reserva = Reserva.findById( req.params.id )         
    .populate('bicicleta')
    .populate('usuario')
    .exec(function(err, reserva) {
        if(err) console.log(err);  
        res.render('reservas/update', {reserva}); 
    }); 
 
}
  
exports.update_post = function(req, res) { 

    var reservaUpdate = {  
        desde:   req.body.desde,
        hasta:   req.body.hasta
    };

    Reserva.findByIdAndUpdate(req.params.id, reservaUpdate, {upsert: true}, ( err, result ) => {
        if (err) console.log(err);
        res.redirect('/reservas');
    }); 
 
}  