var Bicicleta = require('../models/Bicicleta');

exports.bicicleta_list = function(req, res) {
    Bicicleta.allBicis((err, bicicletas) => { 
        res.render('bicicletas/index', {bicis: bicicletas});
    });
}

exports.bicicleta_create_get = function(req, res) {
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res) {
    
     var newBici = {
        "code"     : req.body.code,
        "color"    : req.body.color,
        "modelo"   : req.body.modelo,
        "ubicacion": [ req.body.lat, req.body.lng ]
    };
 
    console.log(newBici);

    Bicicleta.add(newBici, function (err, bici) {
        if (err) return console.log(err);     
        res.redirect('/bicicletas');
    })

}

exports.bicicleta_delete_post = function(req, res) {
 
    var bici = Bicicleta.removeById(req.params.code, function(err, bici) {
        res.redirect('/bicicletas');
    });
}

exports.bicicleta_update_get = function(req, res) { 
    var bici = Bicicleta.findByCode(req.params.code, function(err, bici) {
        res.render('bicicletas/update', {bici} );
    });
}

exports.bicicleta_update_post = function(req, res) {

    var query = { 'code': req.params.code }; 

    var biciUpdate = {
        code:       req.params.code,
        color:      req.body.color,
        modelo:     req.body.modelo,
        ubicacion:  [req.body.lat, req.body.lng]
    };
     
    Bicicleta.findOneAndUpdate(query, biciUpdate, {upsert: true}, function(err, doc) {
        res.redirect('/bicicletas');
    });  
 
}