var Bicicleta = require('../../models/Bicicleta');

exports.bicicleta_list = function(req, res) { 
    // allBicis es la implementacion del find en el modelo de Bicicletas
    Bicicleta.allBicis( function(err, bicis) {
        res.status(200).json( {
            bicicletas: bicis
        });
    });
}

exports.bicicleta_create = function(req, res) {
    
    var bicicleta = new Bicicleta({
        code:   req.body.code, 
        color:  req.body.color,
        modelo: req.body.modelo
    });

    bicicleta.ubicacion = [req.body.lat, req.body.lng];
    
    bicicleta.save(function( err) {
        res.status(200).json(bicicleta);
    });
}

exports.bicicleta_update = function(req, res) {
    console.log(req.params);
    Bicicleta.findByCode(req.params.code, ( err, vBici ) => {
    
        if (vBici === null) {
            res.status(500).json({message: 'ID invalido'});
        }
        else {
            
            var biciUpdate = {
                code:   vBici.code,
                color:  req.body.color,
                modelo: req.body.modelo 
            };

            biciUpdate.ubicacion = [req.body.lat, req.body.lng];
            Bicicleta.updateOne(biciUpdate, ( err, result ) => {
                if (err) console.log(err);
                res.status(200).json(result);
            });
            
        }

    });
}

exports.bicicleta_delete = function(req, res) {
    Bicicleta.removeById(req.params.id);
    res.status(204).send();
}