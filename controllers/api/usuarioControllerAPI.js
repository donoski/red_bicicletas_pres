var Usuario = require('../../models/Usuario');

module.exports = {
    
    list: function(req, res) {
        Usuario.find( {}, function(err, usuarios) {
            if (err) return res.status(500).json(err);
            res.status(200).json( {
                usuarios: usuarios
            });
        });
    },

    create: function(req, res) {
    
        var usuario = new Usuario( { nombre: req.body.nombre,
                                    email: req.body.email,
                                    password: req.body.password,  });

        usuario.save(function(err) {
            res.status(200).json(usuario);
        });

    },

    reservar: function(req, res) {  
        Usuario.findById(req.body.usuario, ( err, usuario ) => { 
            if (err) {
                console.log(err); 
            }
            else {
                usuario.reservar(req.body.bicicleta, req.body.desde, req.body.hasta, ( err ) => { 
                    res.status(200).send();
                });
            }
        }); 

    },

    delete: function(req, res) {
     
        Usuario.removeByUsuario(req.params.nombre);
        res.status(204).send();
        
    },

    update: function(req, res) { 

        var usuarioUpdate = {
            nombre:     req.body.nombre,
            email:      req.body.email,
            password:   req.body.nombre
        };  
        
        Usuario.findByIdAndUpdate(req.params.id, usuarioUpdate, ( err, result ) => {
            if (err) res.status(500);
            res.status(200).json(result);
        });
        
    }

}