var  Reserva = require('../../models/Reserva'); 

exports.list = function( req, res ){
    Reserva.find({}, ( err, reservas ) =>  {
        res.status(200).json({reservas: reservas});
    });
};

exports.update = function(req, res) {
    
    var id = req.body._id;

    Reserva.findById(id, ( err, reserva )=> {
        if( err ) console.log( err );
        
        var reservaUpdate = {
            desde: req.body.desde,
            hasta: req.body.hasta,
            usuario: req.body.usuario,
            bicicleta: req.body.bicicleta
        };

        if(reserva == null) {
            res.status(500).json({error: 'Reserva no encontrada.'});
        } 
        else {
            Reserva.updateOne({ _id: reserva._id }, {$set: reservaUpdate}, function( err, result ){
                res.status(200).json(result);
            });
        }

    });  
};