var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code : Number,
    color : String,
    modelo : String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true }
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){

    return new this({
        code : code,
        color : color,
        modelo : modelo,
        ubicacion : ubicacion
    });

};

bicicletaSchema.methods.toString = function() {
    return "Bicicleta ID: " + this.code + " | Color " + this.color + " | Modelo " + this.modelo;
};

bicicletaSchema.statics.allBicis = function(cb) { 
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(vBici, cb) {
    this.create(vBici, cb);
}

bicicletaSchema.statics.findByCode = function(vCode, cb) { 
    return this.findOne( {code: vCode}, cb );
}

bicicletaSchema.statics.removeById = function(vCode, cb) {
    return this.deleteOne({code: vCode}, cb);
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

