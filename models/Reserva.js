var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;

var reservaSchema = new Schema({
    desde: Date,
    hasta: Date,
    bicicleta: { type: mongoose.Schema.Types.ObjectId, ref: 'Bicicleta' },
    usuario: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario'}
});

reservaSchema.statics.allReservas = function(cb) { 
    return this.find({}, cb);
};

reservaSchema.methods.diasDeReserva = function() {
    return moment(this.hasta).diff(moment(this.desde), 'days') + 1;
}

reservaSchema.statics.updateReserva = function(reserva, cb){
    console.log(reserva);
    this.updateOne({ _id: reserva._id }, {$set: reserva}, cb);
};

reservaSchema.statics.removeById = function(idReserva, cb) { 
    return this.deleteOne({_id: idReserva}, cb);
}

module.exports = mongoose.model('Reserva', reservaSchema);